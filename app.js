var express = require( 'express' );
var app = express();
var server = require( 'http' ).createServer( app );
var io = require( 'socket.io' ).listen( server );
var $Ping = require( './models/Ping' );
var $Website = require( './models/Website' );
/////////////
//	config //
/////////////
//	websockets
io.enable( 'browser client etag' );
io.set( 'log level', 1 );
io.set( 'transports', [ 'websocket', 'flashsocket', 'htmlfile', 'xhr-polling', 'jsonp-polling' ] );
//	webserver
app.configure( function () {
	app.set( 'views', __dirname + '/views' );
	app.set( 'view engine', 'jade' );
	app.use( express.logger( 'dev' ) );
	app.use( express.methodOverride() );
	app.use( app.router );
	app.use( express.static( __dirname + '/public' ) );
} );
/////////////
//	Routes //
/////////////
///
///	landing
app.get( '/', function ( req, res ) {
	$Website.find( function ( err, sites ) {
		if ( err ) {
			res.send( 500, err );
		} else {
			res.render( 'index', {
				sites: sites
			} );
		}
	} );
} );
//	details
app.get( '/:_id/pings', function ( req, res ) {
	$Website.findOne( {
		_id: req.params._id
	} ).exec( function ( err, result ) {
		if ( err ) {
			console.log( err );
			res.send( 500, err );
		} else {
			$Ping.find( {
				url: result.url
			}, function ( err, result ) {
				if ( err ) {
					console.log( err );
					res.send( 500, err );
				} else {
					res.render( 'siteDetails/pings', {
						pings: result,
						url: result[ 0 ].url,
						_id: req.params._id
					} );
				}
			} );
		}
	} );
} );
//////////////////
//	Json routes //
//////////////////
app.get( '/api/:_id/pings', function ( req, res ) {
	$Website.findOne( {
		_id: req.params._id
	} ).exec( function ( err, result ) {
		if ( err ) {
			console.log( err );
			res.send( 500, err );
		} else {
			$Ping.find( {
				url: result.url
			}, function ( err, result ) {
				if ( err ) {
					console.log( err );
					res.send( 500, err );
				} else {
					res.json( result );
				}
			} );
		}
	} );
} );
app.get( '/pings', function ( req, res ) {
	$Ping.find().exec( function ( err, sites ) {
		if ( err ) {
			console.log( err );
			res.send( 500, err );
		} else {
			res.json( sites );
		}
	} );
} );
app.get( '/websites', function ( req, res ) {
	$Website.find( function ( err, result ) {
		if ( err ) {
			console.log( err );
			res.send( 500, err );
		} else {
			res.json( result );
		}
	} );
} );
/////////////////////
//	Socket events //
/////////////////////
io.sockets.on( 'connection', function ( socket ) {
	console.log( 'new socket connection' );
} );
////////////////////
//	Start Server //
////////////////////
server.listen( 3000, function () {
	console.log( 'http server ready on port 3000' );
	var init = function () {
		var Monitors;
		var Ping = require( './lib/Ping' );
		var websites = require( './websites.json' );
		var _ = require( 'underscore' ),
			mongoose = require( 'mongoose' );
		var mongoDbUrl = 'mongodb://localhost/vigil';
		var opts = {
			replSet: {
				rs_name: "rs0"
			}
		};
		var dbConn = mongoose.connect( mongoDbUrl, opts, function ( err ) {
			if ( err ) throw err;
		} );
		var $Ping = require( './models/Ping' );
		var $Website = require( './models/Website' );
		//	find all websites
		//
		$Website.find( function ( err, __websites ) {
			if ( err ) throw err;
			else {
				if ( __websites.length <= 0 ) {
					_.each( websites, function ( site ) {
						var __site = new $Website( site );
						__site.save();
					} );
					websites = __websites;
				} //	Iterate each website configured and begin monitoring
				Monitors = Monitors || _.map( websites, function ( site ) {
					var monitor = new Ping( {
						url: site.url,
						timeout: site.timeout,
						method: site.method
					}, function ( msg ) {
						var __ping = $Ping( {
							url: this._url,
							timeout: this._timeout,
							method: this._method,
							currentStatus: this._currentStatus || 'down',
							upSince: this._upSince,
							recorded: new Date()
						} );
						__ping.save();
					} );
					monitor.ee.on( 'ping', function ( instance ) {
						var __ping = $Ping( {
							url: instance._url,
							timeout: instance._timeout,
							method: instance._method,
							currentStatus: instance._currentStatus || 'down',
							upSince: instance._upSince,
							recorded: new Date(),
							latency: instance._latency
						} );
						io.sockets.volatile.emit( 'ping', __ping );
						__ping.save();
					} );
					monitor.ee.on( 'pingFail', function ( instance ) {
						console.log( 'ping fail', instance );
					} );
					return monitor;
				} );
			}
		} );
	};
	init();
} );