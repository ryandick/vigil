var mongoose = require( 'mongoose' );
var Schema = mongoose.Schema;
var Ping = new Schema( {
	url: {
		type: String,
		required: true
	},
	timeout: {
		type: Number,
		'default': 10000
	},
	method: {
		type: String,
		'default': 'ping'
	},
	currentStatus: {
		type: Boolean,
		'default': false
	},
	upSince: {
		type: Date,
		'default': new Date().getTime()
	},
	recorded: {
		type:Date,
		'default': new Date()
	},
	latency:{
		type: Number,
		'default':0
	}
} );
module.exports = mongoose.model( 'Ping', Ping );
