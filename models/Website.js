var mongoose = require( 'mongoose' );
var Schema = mongoose.Schema;
var Website = new Schema( {
	url: {
		type: String,
		required: true
	},
	timeout: {
		type: Number,
		'default': 200
	},
	method: {
		type: String,
		'default': 'ping'
	},
	name: {
		type: String,
		required: true
	},
	created: {
		type: Date,
		'default': new Date()
	},
	pings: [ {
		type: Schema.Types.ObjectId,
		ref: 'Ping'
	} ]
} );
module.exports = mongoose.model( 'Website', Website );
