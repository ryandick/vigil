var request = require( 'request' );
var EventEmitter =require('events').EventEmitter;

function Ping( options, cb ) {
	this.init( options, cb );
}
Ping.prototype = {

	init: function ( options, cb ) {
		this.ee= new EventEmitter();
		this.ee.setMaxListeners(100);
		this._url = options.url;
		this._timeout = options.timeout || 1000;
		this._method = options.method || 'ping';
		this._currentStatus = false;
		this._upSince = null;
		this._failureCallback = cb;
		this.start();
	},
	start: function () {
		var self = this;
		setInterval( function () {
			self.ping();
		}, self._timeout );
		console.log( '%s is being monitored', this._url );
	},
	ping: function () {
		var self = this;
		self.startTime = new Date().getTime();
		try {
			request( self._url, function ( error, res, body ) {
				if ( !error ) {
					self.endTime = new Date().getTime();
					self.latency = self.endTime - self.startTime;
					self.pingResult( self.latency, res.statusCode );
				} else {
					self._failureCallback( error );
				}
			} );
		} catch ( err ) {
			self._failureCallback( error );
		}
	},
	pingResult: function ( latency, statusCode ) {
		var time = Date.now();
		if ( statusCode === 200 ) {
			if ( !this.currentStatus ) {
				this._upSince = time;
				this._latency = latency;
				this._currentStatus = true;
				this.ee.emit('ping', this);
			}
		} else {
			this._currentStatus = false;
			this._upSince = null;
			this._latency = null;
			this.ee.emit('pingFail', this);
			this._failureCallback( statusCode );
		}
	}
};
module.exports = Ping;
