var io = require('socket.io-client');
var socket = io.connect('http://localhost:3000');

var results = {
    canConnect: false,
    receivedPing:false
};

socket.on('connect',function(){
    console.log('can connect')
    results.canConnect = true;
});

socket.on('ping',function(m){
    console.log(m);
    results.receivedPing = true;
});

while(!results.canConnect && !results.receivedPing){
    setTimeout(function(){
        console.log('waiting...')
    },1);
}